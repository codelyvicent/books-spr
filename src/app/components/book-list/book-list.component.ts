import { Component, OnInit } from '@angular/core';
import { BookPageManager } from 'src/app/models/BookPageManager';
import { Book } from 'src/app/models/Book';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  booksList: Book[] = [];
  bookManagerList: BookPageManager[] = [];
  rendered: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  loadBooks() {
    if (!this.rendered) {
      // Llamará a la API para obtener los libros y sus marcadores
      fetch(environment.apiUrl + '/books')
      .then(response => response.json())
      .then(data => {
        this.insertBooks(data);
        fetch(environment.apiUrl + '/bookPageManagers')
        .then(response => response.json())
        .then(data => { 
          this.associateBooksAndManagers(data)
          this.rendered = !this.rendered;
        })
        .catch(errors => console.log(errors))
      })
      .catch(error => console.log(error));
    }
  }

  savePage(bookPageMan: BookPageManager) {
    fetch(environment.apiUrl + '/bookPageManagers/' + bookPageMan.book.uuid,
    {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({uuid: bookPageMan.book.uuid, currentPage: bookPageMan.currentPage})
    })
    .then(response => response.json())
    .then(data => console.log('OK!'))
    .catch(error => console.error(error))
  }

  associateBooksAndManagers(bookPageManagers: {uuid:string, currentPage:number}[]) {
    //Por cada bookManager, buscamos su libro asociado. Poco óptimo!
    bookPageManagers.forEach( bookPageMan => {
      this.booksList.forEach(book => {
        if (book.uuid === bookPageMan.uuid) {
          this.bookManagerList.push(new BookPageManager(book, bookPageMan.currentPage))
        }
      })
    })
  }

  insertBooks(books: {title: string, author: string, uuid: string}[]){
    books.forEach(book => {
      this.booksList.push(new Book(book.title, book.author, book.uuid));
    })
  }

}
