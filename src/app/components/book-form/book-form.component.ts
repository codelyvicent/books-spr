import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Book } from 'src/app/models/Book';
import { IBook } from 'src/app/interfaces/IBook';
import { environment } from 'src/environments/environment';
import { BookPageManager } from 'src/app/models/BookPageManager';


@Component({
  selector: 'book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {

  registerForm: any;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      title: "",
      author: ""
    })
  }

  registerBook(payload: IBook) {
    let book = new Book(payload.title, payload.author);
    let bookPageManager = new BookPageManager(book);
    fetch(environment.apiUrl + "/books", {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json'
      },
      body: JSON.stringify(book)
  })
      .then(response => response.json())
      .then(data => {
        fetch(environment.apiUrl + "/bookPageManagers", {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({uuid: bookPageManager.book.uuid, currentPage: bookPageManager.currentPage})
        })
      })
      .catch(error => console.error(error));
  }

}
