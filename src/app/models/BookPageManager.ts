import { IBookPageManager } from "../interfaces/IBookPageManager";
import { Book } from "./Book";

export class BookPageManager implements IBookPageManager {

  currentPage: number;

  constructor(public book: Book, currentPage: number=0) {
    this.currentPage = currentPage;
  };

  nextPage(): void {
    this.currentPage = this.currentPage + 1;
  }

  lastPage(): void {
    this.checkInit() ? this.currentPage = this.currentPage - 1 : this.currentPage = 0;
  }

  checkInit(): boolean {
    return this.currentPage > 0 ? true : false;
  }

}