import { IBook } from "../interfaces/IBook";
import { v4 as uuidv4 } from "uuid";

export class Book implements IBook {

  constructor(public title: string, public author: string, public uuid?: string) {
    this.uuid === undefined ? this.uuid = uuidv4() : this.uuid = uuid;
  }

}