# SPR - Books

Proyecto en angular para practicar principios SOLID. Ejemplo con Single Responsibility Principle. Actualmente todo el proyecto está bajo la rama `develop`

## Para desplegar en local

1. Hay un *JSON-SERVER* que hace de base de datos. Arrancarlo como `json-server db.json -i uuid`
2. Arrancar la aplicación con `ng serve -o`. Se abrirá el navegador automáticamente con la aplicación.



